import pika
import sys, os
import base64
import joblib
import pandas as pd
import requests
import json

model = joblib.load('/code/app/python_back_end/models/model.joblib')
reviews = pd.read_csv('/code/app/python_back_end/cleanedData/reviews.csv')
reviews = reviews.head(10000)

def add_datas_to_dataframe(data):
    datas = json.loads(data)
    #Itere sur tous les films
    for x in datas:
        newrow = pd.DataFrame({'uId':x['utilisateurId'],'tconst':x['filmId'], 'rating':x['note'], 'timestamp':x['datePublication']}, index=[0])
        reviews.append(newrow, ignore_index=True)
    #Suppression des doublons sur la cles userId,filmId
    reviews.drop_duplicates(subset=['uId','tconst'], keep='last')
    #Reecriture de reviews.csv (pour import par le modele)
    reviews.to_csv('/code/app/python_back_end/cleanedData/reviews.csv')
    return 

def get_recommandations(user, data):
    #Ajout des données dans notre dataFrame
    add_datas_to_dataframe(data)
    
    my_recs = []
    user_movies = reviews.loc[reviews['uId'] != user, 'tconst'].tolist()
    for tconst in user_movies:
        my_recs.append((tconst, model.predict(user, tconst, r_ui=4).est))
    #10 films avec la meilleure prédiction
    return pd.DataFrame(my_recs, columns=['tconst', 'prediction']).sort_values('prediction', ascending=False).head(10)

def receive():
    connection = pika.BlockingConnection(pika.ConnectionParameters(host='rabbitmq'))
    channel = connection.channel()


    channel.exchange_declare(exchange='logs', exchange_type='fanout')
    channel.queue_declare(queue='Demande_Films')
    
    def callback(ch, method, properties, body):
        userid=body.decode()

        URL = "http://reverseproxy:8080/api/publications/byutilisateur/"+str(userid)
  
        print("URL : ", URL)
        # sending get request and saving the response as response object
        r = requests.get(url = URL)
        


        # extracting data in json format
        data = r.json()


        #TODO RETIRER CA !
        # data='[{"filmId":"tt000001","utilisateurId":"8", "note":8, "datePublication":"2023-01-18T00:00:00"}, {"filmId":"tt000002","utilisateurId":"8", "note":4, "datePublication":"2023-01-18T00:00:00"}]'
        
        # data='[{'filmId': 'tt0120884', 'utilisateurId': 3, 'note': 6, 'commentaire': '', 'datePublication': '2023-01-19T00:00:00'}, {'filmId': 'tt0043618', 'utilisateurId': 3, 'note': 9, 'commentaire': '', 'datePublication': '2023-01-19T00:00:00'}, {'filmId': 'tt0072594', 'utilisateurId': 3, 'note': 4, 'commentaire': '', 'datePublication': '2023-01-19T00:00:00'}]
        
        
        
               
        
        s="["
        for x in data:
            s+=str(x)
            s+=","
        s = s[:-1]
        s+="]"
        s = s.replace("'", "\"")
        data = s
        print(data)
    
        # Loading the model
        listTmp = get_recommandations(userid, data).tconst.values.tolist()
        #listTmp = get_recommandations("ur2183556").tconst.values.tolist()
        
        jsonFilms = '{"idUtilisateur":"'+str(userid)+ '","filmsPredict":["' + '","'.join(listTmp) + '"]}'
        print("JSON BACK PYTHON : ", jsonFilms)
        #Publie les 10 films recuperés
        send("films_recommandes", jsonFilms)
        

    channel.basic_consume(queue='Demande_Films', on_message_callback=callback, auto_ack=True)

    print(' [*] Waiting for messages. To exit press CTRL+C')
    channel.start_consuming()
    return 



def send(nomqueue, _body):
    connection = pika.BlockingConnection(pika.ConnectionParameters('rabbitmq'))
    channel = connection.channel()
    channel.queue_declare(queue=nomqueue)
    channel.basic_publish(exchange='', routing_key=nomqueue, body=_body)
    connection.close()


if __name__ == '__main__':
    try:
        #ecoute une demande de films    
        receive()
   
        
    except KeyboardInterrupt:
        print('Interrupted')
        try:
            sys.exit(0)
        except SystemExit:
            os._exit(0)