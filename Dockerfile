# 
FROM python:3.9

# 
WORKDIR /code

#
COPY ./requirements /code/requirements

# 
RUN pip install --no-cache-dir --upgrade -r /code/requirements

# 
COPY ./RecommandationsMicroService/app /code/app
COPY ./RecommandationsMicroService/python_back_end /code/app/python_back_end

COPY ./run.sh /code/

RUN sed -i 's/\r$//' $app/code/run.sh && chmod +x run.sh

CMD ["/bin/sh", "-c", "/code/run.sh"]

# CMD ["uvicorn", "app.main:app", "--proxy-headers", "--host", "0.0.0.0", "--port", "8080"]
EXPOSE 8080
