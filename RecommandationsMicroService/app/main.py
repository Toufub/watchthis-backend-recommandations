from fastapi import FastAPI, Depends, Request
from fastapi.security import OAuth2PasswordBearer
from pydantic import BaseModel
import jwt
import requests
import json
import base64
import pika
import sys, os
from fastapi.middleware.cors import CORSMiddleware

from pydantic import BaseModel, conlist
from typing import List




app = FastAPI()


oauth2_scheme = OAuth2PasswordBearer(tokenUrl="token", auto_error=False)


origins = [
    "http://89.86.95.28",
    "http://localhost",
    "http://localhost:8080"
    "http://localhost:3000",
]


app.add_middleware(
    CORSMiddleware,
    allow_origins=["*"],
    allow_methods=["*"],
    allow_headers=["*"],
)


@app.get("/")
async def read_main(token: str = Depends(oauth2_scheme)):

    jwt_options = {
        'verify_signature': True,
        'verify_exp': True,
        'verify_nbf': False,
        'verify_iat': True,
        'verify_aud': False
    }

    try:        
        decodedToken=jwt.decode(token, "watchthis is the best movie app !", algorithms="HS256", options=jwt_options)
        y = json.dumps(decodedToken)
        y = json.loads(y)
        userid=y["http://schemas.xmlsoap.org/ws/2005/05/identity/claims/nameidentifier"]
    
    except ValueError:
        print("Erreur lors de la du decodage du token")



    # publish dans l'esb pour dire qu on est pret
    
    send("Demande_Films", userid)

    
    
    
    return decodedToken


def send(nomqueue, _body):
    connection = pika.BlockingConnection(pika.ConnectionParameters('rabbitmq'))
    channel = connection.channel()
    channel.queue_declare(queue=nomqueue)
    channel.basic_publish(exchange='', routing_key=nomqueue, body=_body)
    connection.close()

