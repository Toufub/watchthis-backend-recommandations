from surprise import Reader, Dataset
from surprise.prediction_algorithms import SVD
import pandas as pd 
import joblib
import os

#reviews = pd.read_csv('/cleanedData/reviews.csv')
reviews = pd.read_csv('RecommandationsMicroService/python_back_end/cleanedData/reviews.csv')

#Limitation du dataset pour les performances
small_reviews = reviews.copy().head(10000).reset_index()

reader = Reader()

data = Dataset.load_from_df(small_reviews[['uId', 'tconst', 'rating']], reader) 

algo = SVD()
trainset = data.build_full_trainset()
algo.fit(trainset)

joblib.dump(algo, 'RecommandationsMicroService/python_back_end/models/model.joblib')